# Information / Информация

SPEC-файл для создания RPM-пакета **geary**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/geary`.
2. Установить пакет: `dnf install geary`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)